import os


DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_ARGS = {
    'user': DB_USER,
    'username': DB_USER,
    'password': DB_PASSWORD,
    'host': '127.0.0.1',
    'database':"postgres",
}
