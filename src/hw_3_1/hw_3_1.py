import pandas as pd
import json
from typing import List
from dataclasses import dataclass


path_log = "D:\\hw3\\POS_CASH_balance_plus_bureau-001.log"
path_save_bureau = "D:\\hw3\\bureau.csv"
path_save_poscash = "D:\\hw3\\POS_CASH_balance.csv"


@dataclass
class PosCashBalanceIDs:
    SK_ID_PREV: int
    SK_ID_CURR: int
    NAME_CONTRACT_STATUS: str

@dataclass
class AmtCredit:
    CREDIT_CURRENCY: str
    AMT_CREDIT_MAX_OVERDUE: float
    AMT_CREDIT_SUM: float
    AMT_CREDIT_SUM_DEBT: float
    AMT_CREDIT_SUM_LIMIT: float
    AMT_CREDIT_SUM_OVERDUE: float
    AMT_ANNUITY: float

def normalize_column(df: pd.DataFrame, column: str) -> pd.DataFrame:
    """
    Заменяет колонку со словарем на несколько колонок.
    """
    return pd.concat(
        [
            df,
            pd.json_normalize(df[column]),
        ],
        axis=1,
    ).drop(columns=[column])

def normalize_column_list(name_list: List[dict]) -> pd.DataFrame:

    list_new = pd.DataFrame(name_list)
    name_parse = list_new.columns[1]
    if name_parse == "records":
        list_new = list_new.explode(name_parse).reset_index()
    list_new = normalize_column(list_new, name_parse)
    return list_new

def poshcash_opt(df: pd.DataFrame) -> pd.DataFrame:
    """
    Определение переменной столбца через  аттрибут
    """
    df["PosCashBalanceIDs"] = df["PosCashBalanceIDs"].apply(lambda x: eval(x))
    df["SK_ID_PREV"] = df["PosCashBalanceIDs"].apply(lambda x: x.SK_ID_PREV)
    df["SK_ID_CURR"] = df["PosCashBalanceIDs"].apply(lambda x: x.SK_ID_CURR)
    df["NAME_CONTRACT_STATUS"] = df["PosCashBalanceIDs"].apply(
        lambda x: x.NAME_CONTRACT_STATUS
    )
    df.drop(columns=["PosCashBalanceIDs"], inplace=True)
    return df

def bureau_opt(df: pd.DataFrame) -> pd.DataFrame:
    """
    Определение переменной столбца через  аттрибут
    """
    df["AmtCredit"] = df["AmtCredit"].apply(lambda x: eval(x))
    df["CREDIT_CURRENCY"] = df["AmtCredit"].apply(lambda x: x.CREDIT_CURRENCY)
    df["AMT_CREDIT_MAX_OVERDUE"] = df["AmtCredit"].apply(
        lambda x: x.AMT_CREDIT_MAX_OVERDUE
    )
    df["AMT_CREDIT_SUM"] = df["AmtCredit"].apply(lambda x: x.AMT_CREDIT_SUM)
    df["AMT_CREDIT_SUM_DEBT"] = df["AmtCredit"].apply(lambda x: x.AMT_CREDIT_SUM_DEBT)
    df["AMT_CREDIT_SUM_LIMIT"] = df["AmtCredit"].apply(lambda x: x.AMT_CREDIT_SUM_LIMIT)
    df["AMT_CREDIT_SUM_OVERDUE"] = df["AmtCredit"].apply(
        lambda x: x.AMT_CREDIT_SUM_OVERDUE
    )
    df["AMT_ANNUITY"] = df["AmtCredit"].apply(lambda x: x.AMT_ANNUITY)
    df = df.drop(columns=["AmtCredit"])
    return df


def main(path_log, path_save_bureau, path_save_poscash):

    with open(path_log) as file:
        posh_cash = []
        bureau = []
        for line in file:
            line = json.loads(line)
            if line["type"] == "bureau":
                bureau.append(line["data"])
            else:
                posh_cash.append(line["data"])

    bureau = normalize_column_list(bureau)
    bureau = bureau_opt(bureau)
    bureau.to_csv(path_save_bureau)
    posh_cash = normalize_column_list(posh_cash)
    posh_cash = poshcash_opt(posh_cash)
    posh_cash.to_csv(path_save_poscash)


main(path_log, path_save_bureau, path_save_poscash)
