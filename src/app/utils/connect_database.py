import pandas as pd
import psycopg2
from sqlalchemy.engine import URL
from sqlalchemy import create_engine, text


class Database:
    def __init__(self, db_args: dict):
          self.db_args = db_args
    def send_sql_query(self, query: str):
        """
        Выполняет запрос к базе.

        :param query: строка с sql запросом.
        :param args: аргументы для подключения в БД.
        """
        params = self.db_args.copy()
        del params['username']
        conn = psycopg2.connect(**params)
        try:
            cursor = conn.cursor()
            cursor.execute(query)
            conn.commit()
        except (Exception, psycopg2.Error) as error:
            print("Error while fetching data from PostgreSQL", error)
        finally:
            if conn:
                cursor.close()
                conn.close()

    def get_df_from_query(self, query: str) -> pd.DataFrame:
        """
        Выполняет запрос к базе.

        :param query: строка с sql запросом.
        :param args: аргументы для подключения в БД.

        :return df: датафрейм с результатом.
        """
        params = self.db_args.copy()
        del params['user']
        conn = URL.create('postgresql+psycopg2', **params)
        engine = create_engine(conn)
        df = pd.read_sql(sql=text(query),  con=engine.connect())
        return df
