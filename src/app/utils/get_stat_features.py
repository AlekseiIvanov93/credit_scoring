"""
Скрипт содержит статистические тесты
"""
import pandas as pd
import numpy as np
from scipy.stats import mannwhitneyu, chi2, chi2_contingency, ttest_ind, anderson


def test_anderson(df, feature):
    """
    Тест Андерсона-Дарлинга
    Проверяет гипотезу о нормальном распределении признака

    return: True - нормальное распределение, False - ненормальное распределение.
    """

    for i in df["target"].unique():
        stat, crit_val, sign_level = anderson(df[df["target"] == i][feature], "norm")
        if stat >= crit_val[4]:
            continue
        else:
            return False
        return True


def test_mannwhitneyu(df, feature):
    """
    Тест Манна-Уитни
    Возвращает решение о значимости признака

    return: 1 - признак значимый, 0 - незначимый.
    """

    _, p_mw = mannwhitneyu(
        df[df["target"] == 0][feature], df[df["target"] == 1][feature]
    )
    if p_mw < 0.05:
        return 1
    else:
        return 0


def test_ttest(df, feature):
    """
    t-test
    Возвращает решение о значимости признака

    return: 1 - признак значимый, 0 - незначимый
    """

    _, p_tt = ttest_ind(df[df["target"] == 0][feature], df[df["target"] == 1][feature])
    if p_tt < 0.05:
        return 1
    else:
        return 0


def test_bootstrap(df, feature, func=np.mean, n=1000, alpha=0.05, subtr=np.subtract):
    """
    Бутстрап
    Возвращает решение о значимости признака

    return: 1 - признак значимый, 0 - незначимый
    """

    data1 = df[(df["target"] == 0) & (df[feature].notna())][feature]
    data2 = df[(df["target"] == 1) & (df[feature].notna())][feature]

    s1, s2 = [], []
    s1_size = len(data1)
    s2_size = len(data2)

    for i in range(n):
        itersample1 = np.random.choice(data1, size=s1_size, replace=True)
        s1.append(func(itersample1))
        itersample2 = np.random.choice(data2, size=s2_size, replace=True)
        s2.append(func(itersample2))
    s1.sort()
    s2.sort()

    # доверительный интервал разницы
    bootdiff = subtr(s2, s1)
    bootdiff.sort()
    ci_diff = (
        np.round(bootdiff[np.round(n * alpha / 2).astype(int)], 3),
        np.round(bootdiff[np.round(n * (1 - alpha / 2)).astype(int)], 3),
    )

    cidiff_min = 0.001
    ci_diff_abs = [abs(ele) for ele in ci_diff]
    if min(ci_diff) <= cidiff_min <= max(ci_diff):
        return 0
    elif (cidiff_min >= max(ci_diff_abs) >= 0) or (cidiff_min >= min(ci_diff_abs) >= 0):
        return 0
    else:
        return 1


def test_chi2(df, feature):
    """
    Тест Хи2.
    Возвращает решение о значимости признака

    return: 1 - признак значимый, 0 -  признак незначимый
    """

    cross_tab = pd.concat(
        [
            pd.crosstab(df[feature], df["target"], margins=False),
            df.groupby(feature)["target"].agg(["count", "mean"]).round(4),
        ],
        axis=1,
    ).rename(columns={0: f"target=0", 1: f"target=1", "mean": "probability_of_default"})

    cross_tab["probability_of_default"] = np.round(
        cross_tab["probability_of_default"] * 100, 2
    )

    chi2_stat, p, dof, expected = chi2_contingency(cross_tab.values)
    prob = 0.95
    critical = chi2.ppf(prob, dof)
    if abs(chi2_stat) >= critical:
        return 1
    else:
        return 0
