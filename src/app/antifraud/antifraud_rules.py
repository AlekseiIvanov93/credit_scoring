"""Скрипт для  отсеивание фрода.

Создаются 3 фродовых правила (1-наличие / 0 - отсутсвие):
* fraud_1 - Кол-во открытых кредитов больше 20 и причем нет ни одного закрытого
* fraud_2 - Доля денег которые клиент отдает на займ за год > 1
* fraud_2 - Смена регистрации менее чем за 7 дней до подачи заявки,
         отсутствие мобильного  и рабочего номеров, отсутствие почты.
Наличие хотя бы одного правила - удаление из выборки.
"""
import pandas as pd


def main(train: pd.DataFrame, app_train: pd.DataFrame) -> pd.DataFrame:
    """Скрипт для отсеивание фрода.

    train_df -  Dataframe, который мы сгенерировали на основе признаков в hw_4
    app_train - Dataframe application_train (kaggle)
    """
    train['fraud_1'] = 0
    train.loc[
        (train['cnt_open_credit'] > 20) & (train['cnt_close_credit'] == 0), 'fraud_1',
    ] = 1

    train['fraud_2'] = 0
    train.loc[train['ratio_ann_to_inc'] > 1, 'fraud_2'] = 1

    app_train['fraud_3'] = 0
    app_train.loc[
        (app_train['DAYS_REGISTRATION'] < -7) &
        (app_train['FLAG_MOBIL'] == 0) &
        (app_train['FLAG_EMP_PHONE'] == 0) &
        (app_train['FLAG_EMAIL'] == 0),
        'fraud_3',
    ] = 1
    train['fraud_3'] = app_train['fraud_3']
    train = train.loc[
        (train['fraud_1'] == 0) & (train['fraud_2'] == 0) & (train['fraud_3'] == 0)
    ]
    train = train.drop(columns=['fraud_1', 'fraud_2', 'fraud_3'])
    return train
