"""
Скрипт  для использования StackingClassifier для обученных уже моделей
Скрипт сохраняет модель, submission(kaggle), предобработанный тестовый  датафрейм для данной модели.
"""

import pandas as pd
import numpy as np
import pickle
from numpy import hstack
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import StackingClassifier
from lightgbm import LGBMClassifier
from sklearn.linear_model import LogisticRegression

# train_data/test_data получены через create_featuredata
path_input_train = "D:\\home_credit\\prepro_create_features\\train_data.csv"
path_input_test = "D:\\home_credit\\prepro_create_features\\test_data.csv"
path_to_sub = "D:\\home_credit\\files\\sample_submission.csv"
path_output = "D:\\home_credit\\models\\stack\\"


def main(path_input_train, path_input_test, path_to_sub, path_output):
    train = pd.read_csv(path_input_train)
    test = pd.read_csv(path_input_test)

    train = train.replace([-np.inf, np.inf], [0, 0])
    test = test.replace([-np.inf, np.inf], [0, 0])

    X = train.drop(columns=["target"])
    y = train["target"]

    # Корреляция. Удаление признаков к коэф. >0.85
    cor_matrix = X.corr().abs()
    upper_tri = cor_matrix.where(np.triu(np.ones(cor_matrix.shape), k=1).astype(bool))
    col_drop = [column for column in upper_tri.columns if any(upper_tri[column] > 0.85)]
    X = X.drop(columns=col_drop)
    test = test.drop(columns=col_drop)

    X_train, X_val, y_train, y_val = train_test_split(
        X, y, test_size=0.3, random_state=777, stratify=y
    )
    model_lr = LogisticRegression(
        solver="liblinear", class_weight="balanced", C=0.1, max_iter=100, penalty="l1"
    )
    model_rf = RandomForestClassifier(
        class_weight="balanced", n_jobs=-1, max_depth=9, min_samples_leaf=3
    )
    model_lgbm = LGBMClassifier(
        class_weight="balanced", n_jobs=-1, max_depth=7, min_samples_leaf=2
    )

    estimators = [("lr", model_lr), ("lgbm", model_lgbm), ("rf", model_rf)]

    clf = StackingClassifier(
        estimators=estimators, final_estimator=model_lr, n_jobs=-1, cv=3
    )
    clf.fit(X_train, y_train)
    y_predict = clf.predict_proba(X_val)[:, 1]
    print(f"roc_auc: {round(roc_auc_score(y_val, y_predict), 3)}")

    # Итоговая модель
    clf.fit(X, y)
    model_pkl = path_output + "stack_model.pickle"
    with open(model_pkl, "wb") as file:
        pickle.dump(clf, file)

    # Сохраняем тестовую(kaggle) выборку с оставленными признаками
    test.to_csv(path_output + "test_for_stack.csv", index=False)
    submission = pd.read_csv(path_to_sub)
    # Делаем predict для тестовой(kaggle) выборки
    submission["TARGET"] = clf.predict_proba(test)[:, 1]
    # Сохраняем файл для отправки для kaggle
    submission.to_csv(path_output + "stack_submission.csv", index=False)


main(path_input_train, path_input_test, path_to_sub, path_output)
