"""
Скрипт для использования LGBMClassifier для сгенерированных ранее признаков.
Скрипт сохраняет модель, submission(kaggle), предобработанный тестовый  датафрейм для данной модели.
"""
import pandas as pd
import numpy as np
import pickle
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split, GridSearchCV
from lightgbm import LGBMClassifier


# train_data, test_data получены через скрипт create_featuredata
path_input_train = "D:\\home_credit\\prepro_create_features\\train_data.csv"
path_input_test = "D:\\home_credit\\prepro_create_features\\test_data.csv"
path_to_sub = "D:\\home_credit\\files\\sample_submission.csv"
path_output = "D:\\home_credit\\models\\lgbm\\"


def main(path_input_train, path_input_test, path_to_sub, path_output):
    train = pd.read_csv(path_input_train)
    test = pd.read_csv(path_input_test)

    train = train.replace([-np.inf, np.inf], [0, 0])
    test = test.replace([-np.inf, np.inf], [0, 0])

    X = train.drop(columns=["target"])
    y = train["target"]

    # Корреляция. Удаление признаков к коэф. >0.85
    cor_matrix = X.corr().abs()
    upper_tri = cor_matrix.where(np.triu(np.ones(cor_matrix.shape), k=1).astype(bool))
    col_drop = [column for column in upper_tri.columns if any(upper_tri[column] > 0.85)]
    X = X.drop(columns=col_drop)
    test = test.drop(columns=col_drop)

    X_train, X_val, y_train, y_val = train_test_split(
        X, y, test_size=0.3, random_state=777, stratify=y
    )
    model_lgbm = LGBMClassifier(class_weight="balanced", random_state=777, n_jobs=-1)
    model_lgbm.fit(X_train, y_train)

    # убираем признаки с 0 коэффицентом
    features_import = model_lgbm.feature_importances_
    df_features_import = pd.Series(features_import, index=X_train.columns).sort_values(
        ascending=False
    )
    df_features_import = df_features_import[df_features_import > 0]
    X_new = X[df_features_import.index]
    test_new = test[df_features_import.index]

    # Подбор гиперпараметров
    X_train, X_val, y_train, y_val = train_test_split(
        X_new, y, test_size=0.3, random_state=777, stratify=y
    )
    model_rf = LGBMClassifier(random_state=777, n_jobs=-1)
    params = {
        "max_depth": range(6, 10, 1),
        "min_samples_leaf": [2, 3],
        "class_weight": ["balanced"],
    }
    gs = GridSearchCV(
        model_rf,
        params,
        cv=3,
        scoring="roc_auc",
        return_train_score=True,
        n_jobs=-1,
        verbose=2,
    )
    gs.fit(X_train, y_train)
    best_model = gs.best_estimator_
    print(best_model)
    y_predict = best_model.predict_proba(X_val)[:, 1]
    print(f"roc_auc: {round(roc_auc_score(y_val, y_predict), 3)}")
    print(f"best_params = {gs.best_params_}")

    # Итоговая модель
    best_model.fit(X_new, y)
    model_pkl = path_output + "lgbm_model.pickle"
    with open(model_pkl, "wb") as file:
        pickle.dump(best_model, file)

    # Сохраняем тестовую(kaggle) выборку с оставленными признаками
    test_new.to_csv(path_output + "test_for_lgbm.csv", index=False)
    submission = pd.read_csv(path_to_sub)
    # Делаем predict для тестовой(kaggle) выборки
    submission["TARGET"] = best_model.predict_proba(test_new)[:, 1]
    # Сохраняем файл для отправки для kaggle
    submission.to_csv(path_output + "lgbm_submission.csv", index=False)


main(path_input_train, path_input_test, path_to_sub, path_output)
