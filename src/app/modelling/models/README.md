# create_featuredate
Скрипт создает train_data и test_data  из сгенерированных признаков. Загрузка и сохранение файлов локально

# lr_model
Скрипт для построения модели LogisticRegression. 
* Проверка корреляции признаков. Признаки с коэффициентом больше 0.85  были удалены.
* Стандартизациия данных  StandardScaler()
* Удалены признаки модели коэффициенты которых близки к нулю (1e-4) при l1 регуляризации
* best_params: {'C': 0.1, 'max_iter': 100, 'penalty': 'l1'}
* Отложенная выборка -  roc_auc : 0.7466 
* Рез-т на kaggle
* ![kaggle](lr_kaggle.PNG)

# dt_model
Скрипт для построения модели DecisionTreeClassifier. 
* Проверка корреляции признаков. Признаки с коэффициентом больше 0.85  были удалены.
* Удалены признаки модели при feature_importances_>0
* best_params = {'class_weight': 'balanced', 'criterion': 'entropy', 'max_depth': 6, 'min_samples_leaf': 1}
* Отложенная выборка - roc_auc: 0.7217
* Рез-т на kaggle
* ![kaggle](dt_kaggle.PNG)

# rf_model
Скрипт для построения модели RandomForestClassifier. 
* Проверка корреляции признаков.  Признаки с коэффициентом больше 0.85  были удалены.
* Удалены признаки модели при feature_importances_>0
* best_params = {'class_weight': 'balanced', 'max_depth': 9, 'min_samples_leaf': 3}
* Отложенная выборка - roc_auc: 0.744
* Рез-т на kaggle
* ![kaggle](rf_kaggle.PNG)

# lgbm_model
Скрипт для построения модели LGBMClassifier. 
* Проверка корреляции признаков.  Признаки с коэффициентом больше 0.85  были удалены.
* Удалены признаки модели при feature_importances_>0
* best_params = {'class_weight': 'balanced', 'max_depth': 7, 'min_samples_leaf': 2}
* Отложенная выборка - roc_auc: 0.763
* * Рез-т на kaggle
* ![kaggle](lgbm_kaggle.PNG)

# stack_model
Cкрипт для построения Stacking models 
* Проверка корреляции признаков. Признаки с коэффициентом больше 0.85  были удалены.
* Отложенная выборка - roc_auc: 0.762
* Рез-т на kaggle
* ![kaggle](stack_kaggle.PNG)

# blend_model
Скрипт для построения  Blending models 
* Проверка корреляции признаков. Признаки с коэффициентом больше 0.85  были удалены.
* Обучение LogisticRegression на  мета-признаках  из RandomForestClassifier и LGBMClassifier
* Отложенная выборка - roc_auc: 0.758
* Рез-т на kaggle
* ![kaggle](blend_kaggle.PNG)