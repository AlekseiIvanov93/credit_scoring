"""
Скрипт для отбора  признаков POS_CASH_balance статистическими способами
"""
import pandas as pd
import numpy as np
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS
from src.app.utils.get_stat_features import test_anderson, test_mannwhitneyu, test_ttest, test_bootstrap, test_chi2

np.seterr(divide="ignore", invalid="ignore")


# Путь для сохранения статистически значимых признаков и загрузки локальных файлов
path_save = "D:\\home_credit\\features_stats\\stat_POS_CASH_balance.csv"
path_file_app = "D:\\home_credit\\files\\application_train.csv"
path_file_cash = "D:\\home_credit\\files\\POS_CASH_balance.csv"


def main(path_save, path_file_app, path_file_cash, flag=True, flag_print=False):
    """
    Сохраняет новый датафрейм со статистическими значимыми признаками

    path_save - путь для сохранения датафрейма

    path_file_app - путь для загрузки локального файла application_train
    path_file_cash - путь для загрузки локального файла POS_CASH_balance
    flag - выбор способа загрузки данных
    flag_print - для вывода признаков с неоднозначными стат. тестами

    return: df
    """
    # Если flag==True загрузка данных с БД, иначе работа с локальным файлом
    if flag == True:
        con_db = Database(DB_ARGS)
        sql_select = """SELECT *  FROM POS_CASH_balance"""
        df1 = con_db.get_df_from_query(sql_select)
        df1.columns = [i.lower() for i in df1.columns]

        sql_select = """SELECT SK_ID_CURR, TARGET FROM application_train"""
        df2 = con_db.get_df_from_query(sql_select)
        df2.columns = [i.lower() for i in df2.columns]

    else:
        df1 = pd.read_csv(path_file_cash)
        df1.columns = [i.lower() for i in df1.columns]

        df2 = pd.read_csv(path_file_app)
        df2.columns = [i.lower() for i in df2.columns]
        df2 = df2[["sk_id_curr", "target"]]

    df = df1.merge(df2, how="left", on="sk_id_curr")
    del df1, df2
    columns = df.drop(columns=["sk_id_curr", "target", "sk_id_prev"]).columns.tolist()
    # Признаки со своими метками : 1-признак значимый, 0-признак незначимый
    feature_status = dict()

    # Перебираем признаки, определяем тип данных, проводим стат.тесты и определяем значимость признака
    for feature in columns:
        # Отбор числовых признаков
        if df[feature].dtype != "object":
            data = df[[feature, "target"]].fillna(df[feature].mean())
            # Если признак имеет нормальное распределение(тест Андерсона-Дарлинга)
            if test_anderson(data, feature) == True:
                # Выполнение 2 стат. теста: 1-критерий Манна-Уитни, 2-Bootstrap
                test_1 = test_mannwhitneyu(data, feature)
                test_2 = test_bootstrap(data, feature)
                # Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                if test_1 == test_2:
                    feature_status[feature] = test_1
                # Если рез-ты тестов 1 и 2 не равны, то проводим 3 тест- t-критерий Уэлча
                else:
                    test_3 = test_ttest(data, feature)
                    # Выбираем признак значимомсти  голосованием по большинству среди 1,2,3 тестов
                    lst = [test_1, test_2, test_3]
                    feature_status[feature] = max(lst, key=lst.count)
            # Если признак имеет отличное от нормального распределение
            else:
                # Если признак бинарный:  1 тест - Bootstrap, 2 тест - Хи-квадрат критерий
                if data[feature].nunique() == 2:
                    test_1 = test_bootstrap(data, feature)
                    test_2 = test_chi2(data, feature)
                    # Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                    if test_1 == test_2:
                        feature_status[feature] = test_1
                    else:
                        # Если результаты тестов 1 и 2  не равны, отдаем предпочтение 1 тесту - Bootstrap
                        feature_status[feature] = test_1
                        if flag_print == True:
                            print(f"Признак - {feature} противоречие тестов")
                else:
                    test_1 = test_mannwhitneyu(data, feature)
                    test_2 = test_bootstrap(data, feature)
                    # Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                    if test_1 == test_2:
                        feature_status[feature] = test_1
                    else:
                        # Если результаты тестов 1 и 2  не равны, отдаем предпочтение 2 тесту - Bootstrap
                        feature_status[feature] = test_2
                        if flag_print == True:
                            print(f"Признак - {feature} противоречие тестов")
        # Отбор категориальных признаков
        else:
            data = df[[feature, "target"]]
            # Для категориальных признаков проводим только один тест - Хи-квадрат критерий
            feature_status[feature] = test_chi2(data, feature)

    # Выбираем только стат.значимые признаки
    data = []
    for i in feature_status:
        if feature_status[i] == 1:
            data.append(i)
    # Сохранение
    df[["sk_id_curr", "sk_id_prev", *data]].to_csv(path_save, index=False)


main(path_save, path_file_app, path_file_cash, flag=True, flag_print=False)
