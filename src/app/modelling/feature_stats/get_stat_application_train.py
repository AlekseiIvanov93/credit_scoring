"""
Скрипт для отбора  признаков application_train статистическими способами
"""
import pandas as pd
import numpy as np
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS
from src.app.utils.get_stat_features import test_anderson, test_mannwhitneyu, test_ttest, test_bootstrap,  test_chi2
np.seterr(divide='ignore', invalid='ignore')

#Путь для сохранения статистически значимых признаков и загрузки локального файла
path_save = "D:\\home_credit\\features_stats\\"
path_file = "D:\\home_credit\\files\\"

def main(path_save,path_file, flag=True, flag_print=False):
    """
    Сохраняет новый датафрейм со статистическими значимыми признаками

    path_save - путь для сохранения датафрейма
    path_file - путь для загрузки локального файла
    flag - выбор способа загрузки данных
    flag_print - для вывода признаков с неоднозначными стат. тестами

    return: df
      """
    #Если flag==True загрузка данных с БД, иначе работа с локальным файлом
    if flag==True:
        con_db = Database(DB_ARGS)
        sql_select = '''SELECT * FROM application_train'''
        df_train = con_db.get_df_from_query(sql_select)

        sql_select = '''SELECT * FROM application_test'''
        df_test = con_db.get_df_from_query(sql_select)

    else:
        df_train = pd.read_csv(path_file + "application_train.csv")
        df_test = pd.read_csv(path_file + "application_test.csv")

    df_train.columns = [x.lower() for x in df_train.columns]
    df_test.columns = [x.lower() for x in df_test.columns]
    columns = df_train.drop(columns=["sk_id_curr", "target"]).columns.tolist()
    #Для хранения  статута значимости  признаков: 1-признак значимый, 0-признак незначимый
    feature_status = dict()

    #Перебираем признаки файла, определяем тип данных, проводим стат.тесты и определяем значимость признака
    for feature in columns:
        print(feature)
        #Отбор числовых признаков
        if df_train[feature].dtype != "object":
            data = df_train[[feature, "target"]].fillna(df_train[feature].mean())
            #Если признак имеет нормальное распределение(тест Андерсона-Дарлинга)
            if test_anderson(data, feature) == True:
                #Выполнение 2 стат. теста: 1-критерий Манна-Уитни, 2-Bootstrap
                test_1 = test_mannwhitneyu(data, feature)
                test_2 = test_bootstrap(data, feature)
                #Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                if test_1 == test_2:
                    feature_status[feature] = test_1
                #Если рез-ты тестов 1 и 2 не равны, то проводим 3 тест- t-критерий Уэлча
                else:
                    test_3 = test_ttest(data, feature)
                    #Выбираем признак значимомсти  голосованием по большинству среди 1,2,3 тестов
                    lst = [test_1, test_2, test_3]
                    feature_status[feature] = max(lst, key=lst.count)
            #Если признак имеет отличное от нормального распределение
            else:
                #Если признак бинарный:  1 тест - Bootstrap, 2 тест - Хи-квадрат критерий
                if data[feature].nunique() == 2:
                    test_1 = test_bootstrap(data, feature)
                    test_2 = test_chi2(data, feature)
                    #Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                    if test_1 == test_2:
                        feature_status[feature] = test_1
                    else:
                        #Если результаты тестов 1 и 2  не равны, отдаем предпочтение 1 тесту - Bootstrap
                        feature_status[feature] = test_1
                        if flag_print==True:
                            print(f'Признак - {feature} противоречие тестов')
                else:
                    test_1 = test_mannwhitneyu(data, feature)
                    test_2 = test_bootstrap(data, feature)
                    #Если результаты тестов 1 и 2  равны, сохраняем признак значимости
                    if test_1 == test_2:
                        feature_status[feature] = test_1
                    else:
                        #Если результаты тестов 1 и 2  не равны, отдаем предпочтение 1 тесту - Bootstrap
                        feature_status[feature] = test_2
                        if flag_print == True:
                            print(f'Признак - {feature} противоречие тестов')
        #Отбор категориальных признаков
        else:
            data = df_train[[feature, "target"]]
            #Для категориальных признаков проводим только один тест - Хи-квадрат критерий
            feature_status[feature] = test_chi2(data, feature)

    #Выбираем только стат.значимые признаки
    data = []
    for i in feature_status:
        if feature_status[i] == 1:
            data.append(i)
    #Сохранение
    df_train[["sk_id_curr", "target", *data]].to_csv(path_save + 'stat_application_train.csv', index=False)
    df_test[["sk_id_curr", *data]].to_csv(path_save + 'stat_application_test.csv', index=False)


main(path_save, path_file, flag=True, flag_print=False)
