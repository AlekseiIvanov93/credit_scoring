import pandas as pd
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS


path_save = "D:\\home_credit\\create_features\\credit_card_balance_features.csv"


def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM credit_card_balance"""
    credit_card_balance = con_db.get_df_from_query(sql_select)
    credit_card_balance.columns = [x.lower() for x in credit_card_balance.columns]
    credit_card_balance_features = credit_card_balance[["sk_id_prev", "sk_id_curr"]]

    # Посчитайте все возможные аггрегаты по картам
    cols = ["amt_balance", "amt_credit_limit_actual"]
    agg_cols = credit_card_balance.groupby("sk_id_prev")[cols].agg(
        ["mean", "min", "max"]
    )
    agg_cols.columns = ["_".join(col) for col in agg_cols.columns.values]

    # Посчитайте как меняются аггрегаты. например отношение аггрегата за все время к аггрегату за последние 3 месяца
    # или к данных за последний месяц.
    agg_cols_3month = (
        credit_card_balance.loc[credit_card_balance["months_balance"] <= -3]
        .groupby("sk_id_prev")[cols]
        .agg(["mean", "min", "max"])
    )
    agg_cols_3month.columns = ["_".join(col) for col in agg_cols_3month.columns.values]
    dif_agg = agg_cols / agg_cols_3month
    dif_agg = dif_agg.add_suffix("_div_3")
    dif_agg = dif_agg.fillna(0)
    dif_agg = dif_agg.reset_index()

    agg_cols = agg_cols.reset_index()
    credit_card_balance_features = credit_card_balance_features.merge(
        agg_cols, on="sk_id_prev"
    )
    del agg_cols

    credit_card_balance_features = credit_card_balance_features.merge(
        dif_agg, on="sk_id_prev"
    )
    del dif_agg

    credit_card_balance_features.columns = [x.lower() for x in credit_card_balance_features.columns]
    credit_card_balance_features.to_csv(path_save, index=False)


main(path_save)
