import pandas as pd
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS


path_save = "D:\\home_credit\\create_features\\prev_feat.csv"


def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM previous_application"""
    prev = con_db.get_df_from_query(sql_select)
    prev.columns = [x.lower() for x in prev.columns]
    prev["annuity_credit_ratio"] = prev["amt_annuity"] / prev["amt_credit"]
    prev["app_credit_ratio"] = prev["amt_application"] / prev["amt_credit"]
    prev["credit_goods_ratio"] = prev["amt_credit"] / prev["amt_goods_price"]
    prev["amt_interest"] = (
        prev["cnt_payment"] * prev["amt_annuity"] - prev["amt_credit"]
    )

    col_agg = {
        "amt_annuity": ["min", "max", "mean"],
        "amt_application": ["min", "max", "mean"],
        "amt_credit": ["min", "max", "mean"],
        "amt_down_payment": ["min", "max", "mean"],
        "amt_goods_price": ["min", "max", "mean"],
        "rate_down_payment": ["min", "max", "mean"],
        "days_decision": ["min", "max", "mean"],
        "cnt_payment": ["min", "max", "mean"],
        "annuity_credit_ratio": ["min", "max", "mean"],
        "app_credit_ratio": ["min", "max", "mean"],
        "credit_goods_ratio": ["min", "max", "mean"],
        "amt_interest": ["min", "max", "mean"],
    }

    prev_feat = prev.groupby("sk_id_curr").agg({**col_agg})
    prev_feat.columns = pd.Index(
        [x[1] + "_" + x[0] for x in prev_feat.columns.tolist()]
    )
    prev_feat = prev_feat.reset_index()
    prev_feat.to_csv(path_save, index=False)


main(path_save)
