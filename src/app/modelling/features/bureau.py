import pandas as pd
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS


path_save = "D:\\home_credit\\create_features\\bureau_features.csv"


def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM bureau"""
    bureau = con_db.get_df_from_query(sql_select)
    bureau.columns = [x.lower() for x in bureau.columns]
    bureau_features = bureau[["sk_id_curr", "sk_id_bureau"]]

    #Максимальная сумма просрочки
    max_debt = bureau.groupby("sk_id_curr").agg(max_debt=("amt_credit_sum_debt", "max"))
    bureau_features = bureau_features.merge(max_debt, on="sk_id_curr")
    bureau_features["max_debt"] = bureau_features["max_debt"].fillna(0)

    # Минимальная сумма просрочки
    min_debt = bureau.groupby("sk_id_curr").agg(min_debt=("amt_credit_sum_debt", "min"))
    bureau_features = bureau_features.merge(min_debt, on="sk_id_curr")
    bureau_features["min_debt"] = bureau_features["min_debt"].fillna(0)

    # Какую долю суммы от открытого займа просрочил
    bureau_active = bureau.loc[bureau["credit_active"] == "Active"]
    bureau_features["quota_activ_debt"] = (
        bureau_active["amt_credit_sum_debt"] / bureau_active["amt_credit_sum"]
    )
    bureau_features["quota_activ_debt"] = bureau_features["quota_activ_debt"].fillna(0)

    #Кол-во кредитов определенного типа
    type_credit = (
        pd.pivot_table(
            bureau,
            index="sk_id_curr",
            columns="credit_type",
            values="days_credit_update",
            aggfunc="count",
            fill_value=0,
        )
        .reset_index()
        .add_prefix("count_")
    )
    bureau_features = bureau_features.merge(
        type_credit, left_on="sk_id_curr", right_on="count_sk_id_curr"
    )
    bureau_features = bureau_features.drop("count_sk_id_curr", axis=1)

    # Кол-во просрочек кредитов определенного типа
    bureau["type_expiration"] = 0
    bureau.loc[bureau["credit_day_overdue"] > 1, "is_expired"] = 1
    type_credit_expired = (
        pd.pivot_table(
            bureau,
            index="sk_id_curr",
            columns="credit_type",
            values="type_expiration",
            aggfunc="sum",
            fill_value=0,
        )
        .reset_index()
        .add_prefix("count_exp_")
    )

    bureau_features = bureau_features.merge(
        type_credit_expired, left_on="sk_id_curr", right_on="count_exp_sk_id_curr"
    )
    bureau_features = bureau_features.drop("count_exp_sk_id_curr", axis=1)

    # Кол-во закрытых кредитов определенного типа
    bureau["type_closed"] = 1
    bureau.loc[pd.isna(bureau["days_enddate_fact"]), "type_closed"] = 0
    type_credit_closed = (
        pd.pivot_table(
            bureau,
            index="sk_id_curr",
            columns="credit_type",
            values="type_closed",
            aggfunc="sum",
            fill_value=0,
        )
        .reset_index()
        .add_prefix("count_closed_")
    )

    bureau_features = bureau_features.merge(
        type_credit_closed, left_on="sk_id_curr", right_on="count_closed_sk_id_curr"
    )
    bureau_features = bureau_features.drop("count_closed_sk_id_curr", axis=1)
    bureau_features.columns = [x.lower() for x in bureau_features.columns]
    bureau_features.to_csv(path_save, index=False)


main(path_save)
