import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS


path_save = "D:\\home_credit\\create_features\\"


def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM application_train"""
    app_train = con_db.get_df_from_query(sql_select)
    app_train["flag_data"] = 1

    sql_select = """SELECT * FROM application_test"""
    app_test = con_db.get_df_from_query(sql_select)
    app_test["flag_data"] = 0

    application = pd.concat([app_train, app_test])
    application.columns = [x.lower() for x in application.columns]
    app_feature = application[["sk_id_curr", "flag_data"]].copy(deep=True)
    del app_train, app_test

    # Кол-во документов
    app_feature["cnt_doc"] = application.loc[:, "flag_document_2":"flag_document_21"].sum(axis=1)

    # Есть ли полная информация о доме. Найдите все колонки, которые описывают характеристики дома и
    # посчитайте кол-во непустых характеристик. Если кол-во пропусков меньше 30, то значение признака 1. Иначе 0
    app_feature["flag"] = (
        application.loc[:, "apartments_avg":"emergencystate_mode"].isnull().sum(axis=1)
    )
    app_feature["house_info"] = app_feature["flag"].apply(lambda x: 1 if x < 30 else 0)

    # Кол-во полных лет
    app_feature["age"] = (application.loc[:, "days_birth"] / -365).astype("int")

    # Год смены документа
    app_feature["year_change_doc"] = (
        (application["days_birth"] - application["days_id_publish"]) / -365
    ).astype(int)

    # Разница во времени между сменой документа и возрастом на момент смены документы
    app_feature["diff_year_people_doc"] = (
        app_feature["age"] - app_feature["year_change_doc"]
    )

    # Признак задержки смены документа. Документ выдается или меняется в 14, 20 и 45 лет
    app_feature["doc_delay"] = app_feature["year_change_doc"].apply(
        lambda x: 0 if x == 14 or x == 20 or x == 45 else 1
    )

    # Доля денег которые клиент отдает на займ за год
    app_feature["ratio_ann_to_inc"] = (
        application["amt_annuity"] / application["amt_income_total"]
    )

    # Кол-во взрослых
    application["cnt_adult"] = (
        application["cnt_fam_members"] - application["cnt_children"]
    )

    # Среднее кол-во детей в семье на одного взрослого
    app_feature["avg_cnt_child"] = (
        application["cnt_children"] / application["cnt_adult"]
    )

    # Средний доход на ребенка
    app_feature["avg_inc_child"] = (
        application["cnt_children"]
        * application["amt_income_total"]
        / application["cnt_fam_members"]
    )
    # Средний доход на взрослого
    app_feature["avg_inc_adult"] = (
        application["cnt_adult"]
        * application["amt_income_total"]
        / application["cnt_fam_members"]
    )

    # Процентная ставка
    app_feature["int_rate"] = (
        (application["amt_credit"] - application["amt_goods_price"])
        / ((application["amt_credit"] / application["amt_annuity"]) / 12)
    ) / application["amt_goods_price"]

    # Взвешенный скор внеешних источников. Подумайте какие веса им задать
    app_feature["ext_source_123"] = application[
        ["ext_source_1", "ext_source_2", "ext_source_3"]
    ].mean(axis=1)

    # Поделим людей на группы в зависимости от пола и образования. В каждой группе посчитаем средний доход.
    # Сделаем признак разница между средним доходом в группе и доходом заявителя
    app_feature_train = app_feature.loc[app_feature["flag_data"] == 1].copy(deep=True)
    app_feature_test = app_feature.loc[app_feature["flag_data"] == 0].copy(deep=True)

    train_df = application.loc[
        application["flag_data"] == 1, ["code_gender", "name_education_type", "amt_income_total"]]
    encoder = OneHotEncoder(handle_unknown='ignore')
    encoder_df = pd.DataFrame(encoder.fit_transform(train_df[["code_gender", "name_education_type"]]).toarray())
    final_train_df = train_df.join(encoder_df)
    final_train_df.drop(columns=['code_gender', 'name_education_type'], inplace=True)
    cols_name = final_train_df.drop(columns=['amt_income_total']).columns.tolist()
    final_train_df = final_train_df.groupby(cols_name)["amt_income_total"].transform("mean")

    test_df = application.loc[
        application["flag_data"] == 0, ["code_gender", "name_education_type", "amt_income_total"]]
    encoder_df = pd.DataFrame(encoder.transform(test_df[["code_gender", "name_education_type"]]).toarray())
    final_test_df = test_df.join(encoder_df)
    final_test_df.drop(columns=['code_gender', 'name_education_type'], inplace=True)
    cols_name = final_test_df.drop(columns=['amt_income_total']).columns.tolist()
    final_test_df = final_test_df.groupby(cols_name)["amt_income_total"].transform("mean")

    app_feature_train["diff_inc_group"] = (
        final_train_df
        - application.loc[application["flag_data"] == 1, "amt_income_total"]
    )
    app_feature_test["diff_inc_group"] = (
        final_test_df
        - application.loc[application["flag_data"] == 0, "amt_income_total"]
    )

    app_feature_train = app_feature_train.drop(columns=["flag_data", "flag"])
    app_feature_test = app_feature_test.drop(columns=["flag_data", "flag"])

    app_feature_train.to_csv(path_save + "app_feature_train.csv", index=False)
    app_feature_test.to_csv(path_save + "app_feature_test.csv", index=False)


main(path_save)
