import pandas as pd
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS


path_save = "D:\\home_credit\\create_features\\bureau_bal_feature.csv"


def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM bureau_balance"""
    bureau_bal = con_db.get_df_from_query(sql_select)
    bureau_bal.columns = [x.lower() for x in bureau_bal.columns]
    bureau_bal_feature = pd.DataFrame(
        bureau_bal.drop_duplicates("sk_id_bureau")["sk_id_bureau"]
    )
    table_add_status = bureau_bal.pivot_table(
        index="sk_id_bureau", columns="status", aggfunc="count"
    ).fillna(0)["months_balance"]
    bureau_bal_feature = bureau_bal_feature.merge(table_add_status, on="sk_id_bureau")

    # Кол-во открытых кредитов
    bureau_bal_feature["cnt_open_credit"] = bureau_bal_feature.loc[:, "0":"5"].sum(
        axis=1
    )
    # Кол-во закрытых кредитов
    bureau_bal_feature["cnt_close_credit"] = bureau_bal_feature.loc[:, "C"]

    # Кол-во кредитов
    cnt_credit = bureau_bal.groupby("sk_id_bureau").agg(
        cnt_credit=("sk_id_bureau", "count")
    )
    bureau_bal_feature = bureau_bal_feature.merge(cnt_credit, on="sk_id_bureau")

    # Доля закрытых кредитов
    bureau_bal_feature["quota_close_credit"] = (
            bureau_bal_feature["cnt_close_credit"] / bureau_bal_feature["cnt_credit"]
    )

    # Доля открытых кредитов
    bureau_bal_feature["quota_open_credit"] = (
            bureau_bal_feature["cnt_open_credit"] / bureau_bal_feature["cnt_credit"]
    )

    # Кол-во просроченных кредитов по разным дням просрочки (смотреть дни по колонке STATUS)
    bureau_bal_feature = bureau_bal_feature.rename(
        columns={
            "0": "count_0",
            "1": "count_1",
            "2": "count_2",
            "3": "count_3",
            "4": "count_4",
            "5": "count_5",
        }
    )

    # Доля просроченных кредитов по разным дням просрочки (смотреть дни по колонке STATUS)

    cols_name = [f"count_{i}" for i in range(6)]
    new_cols = [f"quota_{i}_credit" for i in range(6)]
    bureau_bal_feature[new_cols] = bureau_bal_feature[cols_name].divide(bureau_bal_feature[['cnt_credit']].values)

    # Интервал между последним закрытым кредитом и текущей заявкой
    table_add = (
        bureau_bal.loc[bureau_bal["status"] == "C"]
        .groupby("sk_id_bureau")[["months_balance"]]
        .max()
        .reset_index()
    )
    bureau_bal_feature = bureau_bal_feature.merge(
        table_add, how="left", on="sk_id_bureau"
    )
    bureau_bal_feature = bureau_bal_feature.rename(
        columns={"months_balance": "interval_credit_pas"}
    )

    # Интервал между взятием последнего активного займа и текущей заявкой
    table_add = (
        bureau_bal.loc[(bureau_bal["status"] != "C") & (bureau_bal["status"] != "X")]
        .groupby("sk_id_bureau")[["months_balance"]]
        .max()
        .reset_index()
    )
    bureau_bal_feature = bureau_bal_feature.merge(
        table_add, how="left", on="sk_id_bureau"
    )
    bureau_bal_feature = bureau_bal_feature.rename(
        columns={"months_balance": "interval_credit_act"}
    )

    bureau_bal_feature.to_csv(path_save, index=False)


main(path_save)
