import pandas as pd
from src.app.utils.connect_database import Database
from src.config.config import DB_ARGS

path_save = "D:\\home_credit\\create_features\\in_payments_feat.csv"

def main(path_save):
    con_db = Database(DB_ARGS)
    sql_select = """SELECT * FROM installments_payments"""
    in_payments = con_db.get_df_from_query(sql_select)
    in_payments.columns = [x.lower() for x in in_payments.columns]
    in_payments["inst_paym_diff"] = (
        in_payments["amt_instalment"] - in_payments["amt_payment"]
    )
    in_payments["days_diff"] = (
        in_payments["days_entry_payment"] - in_payments["days_instalment"]
    )
    col_agg = {
        "amt_instalment": ["max", "mean", "sum"],
        "amt_payment": ["min", "max", "mean", "sum"],
        "days_entry_payment": ["max", "mean", "sum"],
        "inst_paym_diff": ["min", "max", "mean"],
        "days_diff": ["min", "max", "mean"],
    }
    in_payments_feat = in_payments.groupby("sk_id_curr").agg({**col_agg})
    in_payments_feat.columns = pd.Index(
        [x[1] + "_" + x[0] for x in in_payments_feat.columns.tolist()]
    )
    in_payments_feat.columns = [x.lower() for x in in_payments_feat.columns]
    in_payments_feat = in_payments_feat.reset_index()
    in_payments_feat.to_csv(path_save, index=False)


main(path_save)
